
FROM docker.inteligr8.com/inteligr8/jdk-hotswap:${jdk-hotswap.version}

# Configure the Apache Tomcat Catalina script
ENV JAVA_MEMORY_INIT=128m
ENV JAVA_MEMORY_MAX=512m
ENV CATALINA_HOME="/usr/local/share/tomcat"
ENV CATALINA_BASE="/var/lib/tomcat"
ENV CATALINA_OPTS=

# Download & Install Apache Tomcat
RUN mkdir -p /usr/local/share && \
        curl -L ${tomcat.mirror.baseUrl}/tomcat-${tomcat.majorVersion}/v${tomcat.version}/bin/apache-tomcat-${tomcat.version}.tar.gz -o /usr/local/share/apache-tomcat.tar.gz && \
        cd /usr/local/share && tar xzvf apache-tomcat.tar.gz && mv apache-tomcat-${tomcat.version} tomcat && rm apache-tomcat.tar.gz

# Split Catalina Home & Base
RUN cd /usr/local/share/tomcat && \
        mv webapps webapps.ignore && \
        mkdir -p /var/lib/tomcat/webapps /var/lib/tomcat/lib /var/lib/tomcat/dev && \
        mv conf work temp logs /var/lib/tomcat

# Add directories for dynamic injection points
RUN cd /var/lib/tomcat/dev && \
        mkdir classes classes-extra1 classes-extra2 classes-extra3 classes-extra4 classes-extra5 classes-extra6 classes-extra7 && \
        mkdir post-rsrc post-rsrc-extra1 post-rsrc-extra2 post-rsrc-extra3 && \
        mkdir lib lib-extra1 lib-extra2 lib-extra3 lib-extra4 lib-extra5 lib-extra6 lib-extra7 && \
        mkdir post-lib post-lib-extra1 post-lib-extra2 post-lib-extra3 && \
        mkdir web web-extra1 web-extra2 web-extra3 web-extra4 web-extra5 web-extra6 web-extra7

# Add our Docker container initialization script
ADD maven/target/resources/setenv.sh /usr/local/bin/${namespace.prefix}-setenv.sh
COPY maven/target/resources/docker-entrypoint.sh /usr/local/bin
RUN chmod 755 /usr/local/bin/docker-entrypoint.sh

# Add our Apache Tomcat configuration
#   This gives us dynamic injection points into the running webapps
COPY maven/target/resources/tomcat-context.xml /var/lib/tomcat/conf/context.xml
ADD maven/target/resources/hotswap-agent.properties /var/lib/tomcat/lib

# Listening for HTTP (not HTTPS) traffic
EXPOSE 8080

# Listening for Java debugger traffic
EXPOSE 8000

# Running as ROOT user for now
#USER tomcat

# Execute the Docker container initialization script
ENTRYPOINT [ "/usr/local/bin/docker-entrypoint.sh" ]

# Start the Apache Tomcat web container using the Catalina script
CMD [ "/usr/local/share/tomcat/bin/catalina.sh", "run" ]
