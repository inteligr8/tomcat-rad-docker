
# Apache Tomcat Application Container Image with Hot-Reloading

This project creates a Docker image that has uses the [Java Hotswap Docker Image](/inteligr8/jdk-hotswap-docker) to support hot-reloading for the enablement of rapid application development.  As a Docker image, containers can be started to facilitate development without requiring complicated installations on developer workstations.

It is expected that containers are configured in Maven using the Fabric8 or Spotify Docker plugins.  These configurations should expose the 8080 (HTTP) and 8000 (debugger) ports and any other port that the application may open.  Most importantly, it should mount/bind the source code to the following possible paths.

| Directory                              | Type      | Hot-Reloaded | When |
| -------------------------------------- |:---------:|:------------:|:----:|
| `/var/lib/tomcat/dev/classes`          | Classpath | Yes          | Before WAR |
| `/var/lib/tomcat/dev/classes-extra1`   | Classpath | Yes          | Before WAR |
| `/var/lib/tomcat/dev/classes-extra2`   | Classpath | Yes          | Before WAR |
| `/var/lib/tomcat/dev/classes-extra3`   | Classpath | Yes          | Before WAR |
| `/var/lib/tomcat/dev/classes-extra4`   | Classpath | Yes          | Before WAR |
| `/var/lib/tomcat/dev/classes-extra5`   | Classpath | Yes          | Before WAR |
| `/var/lib/tomcat/dev/classes-extra6`   | Classpath | Yes          | Before WAR |
| `/var/lib/tomcat/dev/classes-extra7`   | Classpath | Yes          | Before WAR |
| `/var/lib/tomcat/dev/post-rsrc`        | Classpath | Yes          | After WAR  |
| `/var/lib/tomcat/dev/post-rsrc-extra1` | Classpath | Yes          | After WAR  |
| `/var/lib/tomcat/dev/post-rsrc-extra2` | Classpath | Yes          | After WAR  |
| `/var/lib/tomcat/dev/post-rsrc-extra3` | Classpath | Yes          | After WAR  |
| `/var/lib/tomcat/dev/lib`              | JARs      | No           | Before WAR |
| `/var/lib/tomcat/dev/lib-extra1`       | JARs      | No           | Before WAR |
| `/var/lib/tomcat/dev/lib-extra2`       | JARs      | No           | Before WAR |
| `/var/lib/tomcat/dev/lib-extra3`       | JARs      | No           | Before WAR |
| `/var/lib/tomcat/dev/lib-extra4`       | JARs      | No           | Before WAR |
| `/var/lib/tomcat/dev/lib-extra5`       | JARs      | No           | Before WAR |
| `/var/lib/tomcat/dev/lib-extra6`       | JARs      | No           | Before WAR |
| `/var/lib/tomcat/dev/lib-extra7`       | JARs      | No           | Before WAR |
| `/var/lib/tomcat/dev/post-lib`         | JARs      | No           | After WAR |
| `/var/lib/tomcat/dev/post-lib-extra1`  | JARs      | No           | After WAR |
| `/var/lib/tomcat/dev/post-lib-extra2`  | JARs      | No           | After WAR |
| `/var/lib/tomcat/dev/post-lib-extra3`  | JARs      | No           | After WAR |
| `/var/lib/tomcat/dev/web`              | Web       | Yes          | Before WAR |
| `/var/lib/tomcat/dev/web-extra1`       | Web       | No           | Before WAR |
| `/var/lib/tomcat/dev/web-extra2`       | Web       | NO           | Before WAR |
| `/var/lib/tomcat/dev/web-extra3`       | Web       | NO           | Before WAR |
| `/var/lib/tomcat/dev/web-extra4`       | Web       | No           | Before WAR |
| `/var/lib/tomcat/dev/web-extra5`       | Web       | No           | Before WAR |
| `/var/lib/tomcat/dev/web-extra6`       | Web       | No           | Before WAR |
| `/var/lib/tomcat/dev/web-extra7`       | Web       | No           | Before WAR |

You may include your own `hotswap-agent.properties` in any of the `classes` folders.  The one loaded by default is usually sufficient.  It most notably disables the Hotswap Agent Hibernate plugin.
